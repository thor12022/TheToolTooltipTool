So, you know that feeling of uncertainty? Is the Metalurgy Tartarite pickaxe better or worse than the Galacticraft Desh pickaxe? That's not even taking into account the Thermal Expansion Invar pickaxe, the Mekanism pickaxes. . . Oh yeah, Mekanism, what about paxels?!

This mod adds one simple thing, a tooltip visible when Advanced Tootips are shown that tells you a tool's Max Durability, Efficiency, Harvest Level and Enchantability. It should even display information for each tool type for multi-tools.

Downloads can be found on  [CurseForge](http://minecraft.curseforge.com/projects/the-tool-tooltip-tool)/[Curse](http://mods.curse.com/mc-mods/minecraft/238734-the-tool-tooltip-tool)

Installing the mod is the same as other Forge mods, drop it into your mods folder and go. It should be compatible with all 1.9 Forge versions from 1887 on. 

You're free to use it in a mod-pack without asking. It would be nice to know if you do though.