package thor12022.thetooltooltiptool;

/*
 * Basic information your mod depends on.
 */

public class ModInformation
{

   public static final String NAME = "The Tool Tooltip Tool";
   public static final String ID = "thetooltooltiptool";
   public static final String CHANNEL = "TheToolTooltipTool";
   public static final String DEPEND = "required-after:Forge@*";
   public static final String VERSION = "%VERSION%";
}
