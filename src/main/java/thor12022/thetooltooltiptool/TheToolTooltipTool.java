package thor12022.thetooltooltiptool;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import thor12022.thetooltooltiptool.client.gui.TooltipHandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = ModInformation.ID, name = ModInformation.NAME, version = ModInformation.VERSION, dependencies = ModInformation.DEPEND)
public class TheToolTooltipTool
{
   @SidedProxy
   public static CommonProxy proxy;

   public static final Logger LOGGER = LogManager.getLogger(ModInformation.NAME);
   
   @Mod.Instance
   public static TheToolTooltipTool instance;

   public static class CommonProxy
   {}
   
   public static class ClientProxy extends CommonProxy
   {
      TooltipHandler eventHandler = new TooltipHandler();
   }
   
   public static class ServerProxy extends CommonProxy
   {
      static
      {
         TheToolTooltipTool.LOGGER.info("DISABLED: Server Environment");
      }
   }
}
