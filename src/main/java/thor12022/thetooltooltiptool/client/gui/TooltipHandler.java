package thor12022.thetooltooltiptool.client.gui;

import java.util.List;
import java.util.Set;
import java.util.Stack;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.init.Blocks;
import net.minecraft.init.Enchantments;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import thor12022.thetooltooltiptool.ModInformation;


public class TooltipHandler
{
   public TooltipHandler()
   {
      MinecraftForge.EVENT_BUS.register(this);
   }
   
   @SubscribeEvent(priority = EventPriority.HIGHEST)
   public void onItemTooltip(ItemTooltipEvent eventArg)
   {  
      if(!eventArg.isShowAdvancedItemTooltips())
      {
         return;
      }
      
      final List<String> tooltip =  eventArg.getToolTip();
      final ItemStack itemStack  =  eventArg.getItemStack();
      
      // Remove the registry name and NBT Tags lines
      final Stack<String> removedLines = new Stack<>();
      removedLines.add(tooltip.remove(tooltip.size() - 1));
      if(itemStack.hasTagCompound())
      {
         removedLines.add(tooltip.remove(tooltip.size() - 1));
      }
    
      if(itemStack.isItemStackDamageable())
      {
         if(!itemStack.isItemDamaged())
         {
            final int maxDamageMultiplier = EnchantmentHelper.getEnchantmentLevel(Enchantments.UNBREAKING, itemStack) + 1;
            if( maxDamageMultiplier == 1)
            {
               tooltip.add(new String(TextFormatting.GRAY + 
                                      I18n.format(ModInformation.ID + ".tooltip.maxDamage")+": " + 
                                      itemStack.getItem().getMaxDamage()));
            }
            else
            {
               tooltip.add(new String(TextFormatting.GRAY + 
                                      I18n.format(ModInformation.ID + ".tooltip.averageMaxDamage")+": " + 
                     itemStack.getItem().getMaxDamage() * maxDamageMultiplier));
            }
         }
      }
      
      if(itemStack.getItem() instanceof ItemTool)
      {
         final float extraEfficiency = ( EnchantmentHelper.getEnchantmentLevel(Enchantments.EFFICIENCY, itemStack) * 0.3f ) + 1;
         final Set<String> toolClasses = itemStack.getItem().getToolClasses(itemStack);
         for(String toolClass : toolClasses)
         {
            tooltip.add(new String(TextFormatting.GRAY + 
                                   I18n.format(ModInformation.ID + ".tooltip.harvestLevel") + 
                                   " (" + I18n.format(ModInformation.ID + ".tooltip." + toolClass) + "): " + 
                                   itemStack.getItem().getHarvestLevel(itemStack, toolClass)));
            if( toolClass.equals("pickaxe") || toolClass.equals("paxel") )
            {
               final IBlockState blockState = Blocks.STONE.getDefaultState();
               tooltip.add(new String(TextFormatting.GRAY + 
                                      I18n.format(ModInformation.ID + ".tooltip.digSpeed") + 
                                      " (" + I18n.format(ModInformation.ID + ".tooltip." + toolClass) + "): " + 
                                      String.format("%.1f", itemStack.getItem().getStrVsBlock(itemStack, blockState) * extraEfficiency)));
            }
            else if( toolClass.equals("shovel") )
            {
               final IBlockState blockState = Blocks.DIRT.getDefaultState();
               tooltip.add(new String(TextFormatting.GRAY + 
                                      I18n.format(ModInformation.ID + ".tooltip.digSpeed") +
                                      " (" + I18n.format(ModInformation.ID + ".tooltip." + toolClass) + "): " + 
                                      String.format("%.1f", itemStack.getItem().getStrVsBlock(itemStack, blockState) * extraEfficiency)));
            }else if( toolClass.equals("axe") )
            {
               final IBlockState blockState = Blocks.LOG.getDefaultState();
               tooltip.add(new String(TextFormatting.GRAY + 
                                      I18n.format(ModInformation.ID + ".tooltip.digSpeed") +
                                      " (" + I18n.format(ModInformation.ID + ".tooltip." + toolClass) + "): " + 
                                      String.format("%.1f", itemStack.getItem().getStrVsBlock(itemStack, blockState) * extraEfficiency)));
            } 
         }  
      }
      
      if(itemStack.isItemEnchantable() || itemStack.isItemEnchanted())
      {
         tooltip.add(new String(TextFormatting.GRAY + 
                                I18n.format(ModInformation.ID + ".tooltip.enchantability") + ": " + 
                                itemStack.getItem().getItemEnchantability()));
      }
      
      while(!removedLines.isEmpty())
      {
         tooltip.add(removedLines.pop());
      }
   }
}
